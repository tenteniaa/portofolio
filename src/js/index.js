const navbar = document.getElementsByTagName('nav')[0];
const content = document.getElementsByClassName('up-content')[0];

window.addEventListener('scroll', function() {
    if (window.scrollY > 1) {
        navbar.classList.replace('bg-white', 'nav-color');
        content.classList.replace('d-none', 'up-appear');
    } else if (this.window.scrollY <= 0) {
        navbar.classList.replace('nav-color','bg-white');
        content.classList.replace('up-appear', 'd-none');
    }
});

/* Scroll Active Link */
const li = document.querySelectorAll('.nav__item');
const sec = document.querySelectorAll('section');

function activeMenu() {
    let len=sec.length;
    while(--len && window.scrollY + 97 < sec[len].offsetTop){};
    li.forEach(ltx => ltx.classList.remove('nav__active'));
    li[len].classList.add('nav__active');
}
activeMenu();
window.addEventListener('scroll', activeMenu);